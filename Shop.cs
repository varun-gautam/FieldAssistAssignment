using System;
using System.Collections.Generic;


namespace OnilneShop
{
    public static class Shop
    {
      //ShopFunctions Private Variables
      private static List<PrimaryCategory> allPrimaryCategorys;
      private static List<SecondaryCategory> allSecondaryCategorys;
      private static List<Product> allProducts;
      private static List<IScheme> allSchemes;
      
      //ShopFunctions Public Properties
      public static List<PrimaryCategory> allPrimaryCategorysList
      {
        get
        {
          return allPrimaryCategorys;
        }
      }
      public static List<SecondaryCategory> allSecondaryCategorysList
      {
        get
        {
          return allSecondaryCategorys;
        }
      }
      public static List<Product> allProductsList
      {
        get
        {
          return allProducts;
        }
      }
      public static List<IScheme> allSchemesList
      {
        get
        {
         return allSchemes; 
        }
      }
      
      //Constructor
      static Shop()
      {
        allPrimaryCategorys = new List<PrimaryCategory>();
        allSecondaryCategorys = new List<SecondaryCategory>();
        allProducts = new List<Product>();
        allSchemes = new List<IScheme>();
      }
      
      //ShopFunctions Public Methods
      public static void AttachScheme(IScheme scheme, ISchemeAttachable products)
      {
        if(scheme.endDate<DateTime.Now)
        {
            scheme.expired = true;
        }
        
        if((!scheme.expired) && (scheme.startDate>DateTime.Now))
        {
            products.schemeAttached.Add(scheme);
        }
      }
      
      public static PrimaryCategory CreatePrimaryCategory(string primaryCatID, string primaryCatDetails)
      {
        PrimaryCategory obj=new PrimaryCategory(primaryCatID,primaryCatDetails); 
        allPrimaryCategorys.Add(obj);
        return obj;
      }
      
      public static SecondaryCategory CreateSecondaryCategory(string primaryCatID, PrimaryCategory primaryCat, string primaryCatDetails)
      {
        SecondaryCategory obj=new SecondaryCategory(primaryCatID,primaryCat,primaryCatDetails); 
        allSecondaryCategorys.Add(obj);
        return obj;
      }
      
      public static Product CreateProduct(string prodID, string prodName, SecondaryCategory secondaryCat, string prodDetails, decimal price)
      {
        Product obj=new Product(prodID,prodName,secondaryCat,prodDetails,price); 
        allProducts.Add(obj);
        return obj;
      }
      
      public static ArticleScheme CreateArticleScheme(bool exp , DateTime stDate , DateTime endDate, int totalQtyBooked , decimal minRevenue , int minQuanity , List<Tuple<Product, int>> prdFreeOfCost)
      {
        ArticleScheme obj = new ArticleScheme(exp , stDate , endDate, totalQtyBooked , minRevenue ,  minQuanity ,  prdFreeOfCost );
        allSchemes.Add((IScheme) obj);
        return obj;
      }
      
      public static DiscountScheme CreateDiscountScheme(bool exp , DateTime stDate , DateTime endDate, int totalQtyBooked , decimal minRevenue , int minQuanity , List<Tuple<Product, int>> prdFreeOfCost , decimal discountPer)
      {
        DiscountScheme obj = DiscountScheme( exp ,  stDate ,  endDate,  totalQtyBooked , minRevenue ,  minQuanity ,  prdFreeOfCost , discountPer);
        allSchemes.Add((IScheme) obj);
        return obj;
      }
      
      public static void ChangeSecondaryCategoryOfProduct(Product product, SecondaryCategory secondaryCat)
      {
        product.secondaryCategory = secondaryCat;
      }
      
      public static void ChangePrimaryCategoryOfProduct(SecondaryCategory secondaryCat, PrimaryCategory primaryCat)
      {
        secondaryCat.primaryCategory = primaryCat;
      }
      
      public static void ExpireScheme(IScheme scheme)
      {
        scheme.expired = true;
      }
      
      
    }
}