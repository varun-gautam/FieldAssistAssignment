using System;
using System.Collections.Generic;
using System.Type;

namespace OnilneShop
{
    class ShoppingCart
    {
      //ShoppingCart Public Properties
      public List<Tuple<Product, int>> cartProducts { get; set; } //Product and Quantity
      public decimal billAmount {get; set;}
      public List<Tuple<Product, int>> cartSchemeProducts { get; set; } // Free of Cost Products and Quantity
      
      
      public List<IScheme> applicableSchemes
      {
        get
        {
          List<IScheme> schemesApplicable = new List<IScheme>();
        foreach (Tuple<Product, int> productItem in cartProducts)
        {
          Product product = productItem.Item1;
          if(product.schemeAttached.Count !=0)
          {
            foreach ( IScheme schemesAttached in product.schemeAttached) 
          {
            if((!schemesAttached.expired) && (schemesAttached.endDate>DateTime.Now) && (schemesAttached.startDate<DateTime.Now) )
            {
              schemesApplicable.Add(schemesAttached);
            }
          }
          }
          if(product.secondaryCategory.schemeAttached.Count !=0)
          {
            foreach ( IScheme schemesAttached in product.secondaryCategory.schemeAttached) 
          {
            if((!schemesAttached.expired) && (schemesAttached.endDate>DateTime.Now) && (schemesAttached.startDate<DateTime.Now) )
            {
              schemesApplicable.Add(schemesAttached);
            }
          }
          }
          if(product.secondaryCategory.primaryCategory.schemeAttached.Count !=0)
          {
            foreach ( IScheme schemesAttached in product.secondaryCategory.primaryCategory.schemeAttached) 
          {
            if((!schemesAttached.expired) && (schemesAttached.endDate>DateTime.Now) && (schemesAttached.startDate<DateTime.Now) )
            {
              schemesApplicable.Add(schemesAttached);
            }
          }
          }
         
        }
           return schemesAttached;
          
        }
      }
        
      public bool articleSchemeApplicable
      {
        get
        {
        foreach(IScheme scheme in applicableSchemes)
        {
          if("ArticleScheme" == typeof(scheme).Name)
          {
            return true;
          }
        }
        return false;
        }
      }
      
      public Tuple<decimal, int> revenueQuantity
      {
        get
        {
          decimal revenueGenerated = 0;
          int quantity = 0;
          foreach (Tuple<Product, int> productItem in cartProducts)
          {
             Product product = productItem.Item1;
             quantity += productItem.Item2;
             revenueGenerated += ((product.Price)*(productItem.Item2));
          }
          Tuple<decimal, int> revenueQty =new Tuple<decimal, int>(revenueGenerated, quantity);
          return revenueQty;
        }
      }
      
      public List<Tuple<IScheme, decimal>> SchemeBenefit
      {
        get
        {
        List<Tuple<IScheme, decimal>> schemeBenefit = new List<Tuple<IScheme, decimal>>();
        decimal benefit = 0;
        decimal revenueGenerated = revenueQuantity.Item1;
        int quantity = revenueQuantity.Item2;
        foreach (IScheme scheme in applicableSchemes)
        {
          benefit = scheme.SchemeBenefit(revenueGenerated,quantity);
          schemeBenefit.Add(new Tuple<IScheme, decimal>(scheme, benefit));
        }
        return schemeBenefit;
        }
      }
      
      //ShoppingCart Private Properties
      private List<IScheme> attachedSchemes
      {
        get
        {
        List<IScheme> schemesAttached = new List<IScheme>();
        foreach (Tuple<Product, int> productItem in cartProducts)
        {
          Product product = productItem.Item1;
          if(product.schemeAttached.Count !=0)
          {
          schemesAttached.AddRange(product.schemeAttached);
          }
          if(product.secondaryCategory.schemeAttached.Count !=0)
          {
            schemesAttached.AddRange(product.secondaryCategory.schemeAttached);
          }
          if(product.secondaryCategory.primaryCategory.schemeAttached.Count !=0)
          {
            schemesAttached.AddRange(product.secondaryCategory.primaryCategory.schemeAttached);
          }
         
        }
           return schemesAttached;
        }
      }
      
      //Constructor
      public ShoppingCart()
      {
        cartProducts = new List<Tuple<Product, int>>();
        cartSchemeProducts = new List<Tuple<Product, int>>();
      }
      
      //ShoppingCart Public Methods
      public void AddProductInCart(Product prod, int quantity)
      {
        
        foreach (Tuple<Product, int> productItem in cartProducts)
        {
          if(productItem.Item1 == prod)
          {
            productItem.Item2 += quantity;
            return;
          }
        }
        
        cartProducts.Add(new Tuple<Product, int>(prod, quantity));
        
      }
      
      public void AvailSchemeBenefitCart()
      {
        int quantity = revenueQuantity.Item2;
        billAmount = revenueQuantity.Item1;
        foreach(IScheme scheme in applicableSchemes)
        {
           if("ArticleScheme" == typeof(scheme).Name)
           {
            if(  scheme.SchemeBenefit( revenue,  quantity) > 0)
            {
              cartSchemeProducts.AddRange(scheme.productsFreeOfCostQuantity);
              scheme.AvailSchemeBenefit(quantity);
              
            }
            else
            {
              
            }
           }
          else if("DiscountScheme" == typeof(scheme).Name)
          {
            if(  scheme.SchemeBenefit( revenue,  quantity) > 0)
            {
              billAmount = scheme.SchemeBenefit( billAmount,  quantity);
              scheme.AvailSchemeBenefit(quantity);
              
            }
          }
        }
      }
      
      
      
    }
}