using System;
using System.Collections.Generic;

namespace OnilneShop
{
  public class DiscountScheme : IScheme
  {
     //IScheme defined  properties
     public bool expired { get; set;}
     public DateTime startDate  { get; set; }
     public DateTime endDate  { get; set; }
     public int totalQuantityBooked { get; set; }
     public decimal minimumRevenue { get; set; }
     public int minimumQuanity { get; set;}
     public List<Tuple<Product, int>> productsFreeOfCostQuantity { get; set;}
     
     //Discount Scheme Public Properties
     public decimal discountPercentage { get; set; }
     
     //Constructor
     public DiscountScheme(bool exp , DateTime stDate , DateTime edDate, int totalQtyBooked , decimal minRevenue , int minQuanity , List<Tuple<Product, int>> prdFreeOfCost , decimal discountPer)
     {
      expired = exp;
      startDate = stDate;
      endDate = edDate;
      totalQuantityBooked = totalQtyBooked;
      minimumRevenue = minRevenue;
      minimumQuanity = minQuanity;
      productsFreeOfCostQuantity = prdFreeOfCost;
      discountPercentage = discountPer;      
     }
     
     //IScheme Public methods
     public decimal SchemeBenefit(decimal revenueGenerated, int quantity)
     {
     decimal valueOfDiscount = 0;
     if(revenueGenerated>=minimumRevenue || quantity>=minimumQuanity)
     {
        valueOfDiscount = Decimal.Divide( (Decimal.Multiply(revenueGenerated,discountPercentage)), 100);
     }
     if(endDate>DateTime.Now)
        {
            scheme.expired = true;
            valueOfDiscount = 0;
        }
     return valueOfDiscount;
     }
     
     public void AvailBenefit(int quantity)
     {
        if(scheme.endDate>DateTime.Now)
        {
            scheme.expired = true;
        }
        else
        {
        totalQuantityBooked += quantity;
        }
     }
     
  }
}