using System;
using System.Collections.Generic;


namespace OnilneShop
{
    class Product : ISchemeAttachable, IEquatable<Product>
    {
    
      //ISchemeAttachable properties
      public List<IScheme> schemeAttached {get; set;}
    
      //Product properties
      public string productID { get; set; } //SKU id
      public string productName { get; set; }
      public SecondaryCategory secondaryCategory { get; set;}      
      public string productDetails { get; set; }
      public decimal price { get; set; }
      
      //constructors
      public Product()
      {
      schemeAttached = new List<IScheme>();
      price = 0;
      }
      
      public Product(string prodID,string prodName, SecondaryCategory secondaryCat, string prodDetails, decimal pricee)
      {
      schemeAttached = new List<IScheme>();
      productID = prodID;
      productName = prodName;
      secondaryCategory = secondaryCat;
      productDetails = prodDetails;
      price = pricee;
      }
      
      //IEquatable Methods
        public bool Equals(Product other)
         {
            if (other == null)
              {
                return false;
              }

            if (this.productID == other.productID)
              {
                return true;
              }
            else
              {
                return false;
              }
        }
      
      //Public Methods
      public void ChangeSecondaryCategoryID(SecondaryCategory secondaryCat)
      {
      secondaryCategory = secondaryCat;
      }
      
    }
}