using System;
using System.Collections.Generic;


namespace OnilneShop
{
    class PrimaryCategory : ISchemeAttachable
    {
      //ISchemeAttachable properties
      public List<IScheme> schemeAttached {get; set;}
      
      //PrimaryCategory Public properties
      public string primaryCategoryID { get; set; }
      public string primaryCategoryDetails { get; set;}      
      
      
      //constructors
      public PrimaryCategory()
      {
      schemeAttached = new List<IScheme>();
      }
      
      public PrimaryCategory(string primaryCatID, string primaryCatDetails)
      {
      schemeAttached = new List<IScheme>();
      primaryCategoryID = primaryCatID;
      primaryCategoryDetails = primaryCatDetails;
      }
      
    }
}