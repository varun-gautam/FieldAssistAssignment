using System;

namespace OnilneShop
{
  public interface ISchemeAttachable
  {
    List<IScheme> schemeAttached { get; set; }
  }
}