using System;
using System.Collections.Generic;

namespace OnilneShop
{
  public class ArticleScheme : IScheme
  {
     //IScheme defined  properties
     public bool expired { get; set;}
     public DateTime startDate  { get; set; }
     public DateTime endDate  { get; set; }
     public int totalQuantityBooked { get; set; }
     public decimal minimumRevenue { get; set; }
     public int minimumQuanity { get; set;}
     public List<Tuple<Product, int>> productsFreeOfCostQuantity{ get; set;}
     
     //Article Scheme Public Properties
     
     //Constructor
     public ArticleScheme(bool exp , DateTime stDate , DateTime edDate, int totalQtyBooked , decimal minRevenue , int minQuanity , List<Tuple<Product, int>> prdFreeOfCost )
     {
      expired = exp;
      startDate = stDate;
      endDate = edDate;
      totalQuantityBooked = totalQtyBooked;
      minimumRevenue = minRevenue;
      minimumQuanity = minQuanity;
      productsFreeOfCostQuantity = prdFreeOfCost;     
     }
     
     //IScheme Public Methods
     public decimal SchemeBenefit(decimal revenueGenerated, int quantity)
     {
      
     decimal applicable = 0;
     if(revenueGenerated>=minimumRevenue || quantity>=minimumQuanity)
     {
        applicable = 1;
     }
     if(endDate>DateTime.Now)
        {
            scheme.expired = true;
            applicable = 0;
        }
     return applicable;
     
     } 
     
     public void AvailBenefit(int quantity)
     {
      if(scheme.endDate>DateTime.Now)
        {
            scheme.expired = true;
        }
        else
        {
        totalQuantityBooked += quantity;
        }
     }
  }
}